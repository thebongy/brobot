from telegram.ext import Updater, Filters, MessageHandler, ConversationHandler, CommandHandler
import os
from unknownHandler import unknown, wrongOption
from startHandler import start
from memeHandler import meme
from jokeHandler import joke
from eventHandler import optionSelection, processOption, registration, answerSubmission
from coronaHandler import countrySelection, dateSelection, coronaupdates


token = os.getenv("TOKEN")
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher
joke_handler = CommandHandler('joke', joke)
dispatcher.add_handler(joke_handler)
corona_states = {0: [MessageHandler(Filters.text, dateSelection)], 1: [
    MessageHandler(Filters.text, coronaupdates)]}
corona_handler = ConversationHandler(
    entry_points=[
        CommandHandler(
            'coronavirus',
            countrySelection)],
    states=corona_states,
    fallbacks=[
        MessageHandler(
            Filters.all,
            wrongOption)])
dispatcher.add_handler(corona_handler)
meme_handler = CommandHandler('meme', meme)
dispatcher.add_handler(meme_handler)
start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)
updates_states = {
    0: [
        MessageHandler(
            Filters.regex('^(Leaderboard|SubmitAnswer|MyScore|Register|GetHint)$'), processOption)], 1: [
                MessageHandler(
                    Filters.text, registration)], 2: [
                        MessageHandler(
                            Filters.text, answerSubmission)]}
updates_handler = ConversationHandler(
    entry_points=[
        CommandHandler(
            'updates', optionSelection)], states=updates_states, fallbacks=[
                MessageHandler(
                    Filters.all, wrongOption)])
dispatcher.add_handler(updates_handler)
unknown_handler = MessageHandler(Filters.all, unknown)
dispatcher.add_handler(unknown_handler)
updater.start_polling()
