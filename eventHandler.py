from telegram import ReplyKeyboardMarkup
from telegram.ext import ConversationHandler
from dbInit import user, question
import time
from text import alreadyregistered, selectusername, askanswer, submittedall, registerfirst, invalidmessage, usernametaken, registersuccess, gamecompleted, wronganswer


def optionSelection(update, context):
    options = [["Leaderboard"], [
        "SubmitAnswer"], ["Register"], ["MyScore"], ["GetHint"]]
    update.message.reply_text(
        "Choose one of the following category:",
        reply_markup=ReplyKeyboardMarkup(
            options,
            one_time_keyboard=True,
            resize_keyboard=True,
            selective=True))
    return 0


def processOption(update, context):
    selection = update.message.text
    if selection == "Register":
        users = user.find_one({"email": update.message.from_user.username})
        if users:
            message = alreadyregistered
            update.message.reply_text(message)
            return ConversationHandler.END
        else:
            message = selectusername
            update.message.reply_text(message)
            return 1
    elif selection == "SubmitAnswer":
        users = user.find_one({"email": update.message.from_user.username})
        if users:
            if question.find_one({"Id": users["points"]}):
                message = askanswer
                update.message.reply_text(message)
                return 2
            else:
                message = submittedall
                update.message.reply_text(message)
                return ConversationHandler.END
        else:
            message = registerfirst
            update.message.reply_text(message)
            return ConversationHandler.END
    elif selection == "MyScore":
        users = user.find_one({"email": update.message.from_user.username})
        if users:
            message = str(users["points"])
        else:
            message = registerfirst
        update.message.reply_text(message)
        return ConversationHandler.END
    elif selection == "Leaderboard":
        users = {}
        for i in user.find():
            if str(i["points"]) in users:
                users[str(i["points"])][str(i["timestamp"])] = i["username"]
            else:
                users[str(i["points"])] = {
                    str(i["timestamp"]): i["username"]
                }
        userPoints = reversed([str(i)
                               for i in sorted([int(i) for i in users.keys()])])
        leaderBoard = []
        for i in userPoints:
            for j in [str(j) for j in sorted([float(j)
                                              for j in users[i].keys()])]:
                leaderBoard.append([users[i][j], i])
        message = ""
        for i in leaderBoard[:10]:
            message += f"{i[0]}: {int(i[1])}\n"
        message = message.strip()
        update.message.reply_text(message)
        return ConversationHandler.END
    elif selection == "GetHint":
        users = user.find_one({"email": update.message.from_user.username})
        if users:
            if users["points"] == 1:
                message = firsthint
            elif question.find_one({"Id": users["points"] - 1})["NextAccount"]:
                message = "Clue: " + \
                    question.find_one(
                        {"Id": users["points"] - 1})["NextAccount"] + "."
            else:
                message = submittedall
        else:
            message = registerfirst
        update.message.reply_text(message)
        return ConversationHandler.END
    else:
        message = invalidmessage
        update.message.reply_text(message)
        return ConversationHandler.END


def registration(update, context):
    username = update.message.text
    users = user.find_one({"username": username})
    if users:
        message = usernametaken
        update.message.reply_text(message)
        return ConversationHandler.END
    else:
        user.insert_one({"email": update.message.from_user.username,
                         "username": username,
                         "points": 1,
                         "timestamp": float(int(time.time() * 1000)),
                         "password": "N/A"})
        message = registersuccess
        update.message.reply_text(message)
        return ConversationHandler.END


def answerSubmission(update, context):
    answer = update.message.text
    users = user.find_one({"email": update.message.from_user.username})
    if answer.lower() == question.find_one(
            {"Id": users["points"]})["Answer"].lower():
        if question.find_one({"Id": users["points"]})["NextAccount"]:
            message = "That is correct! Next clue: " + \
                question.find_one({"Id": users["points"]})["NextAccount"] + "."
        else:
            message = gamecompleted
        user.update_one(
            users, {
                "$inc": {
                    "points": 1}, "$set": {
                    "timestamp": float(int(
                        time.time() * 1000))}})
    else:
        message = wronganswer
    update.message.reply_text(message)
    return ConversationHandler.END
