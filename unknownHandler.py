import requests
from urllib.parse import quote_plus
from telegram.ext import ConversationHandler
from text import invalidmessage, unknownmessage


def unknown(update, context):
    try:
        text = quote_plus(update.message.text)
        api = f"https://rahil-brobot.herokuapp.com/?query={text}"
        response = requests.get(api).json()
        message = response["response"]
        update.message.reply_text(message)
    except BaseException:
        message = unknownmessage
        update.message.reply_text(message)


def wrongOption(update, context):
    message = invalidmessage
    update.message.reply_text(message)
    return ConversationHandler.END
