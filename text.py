startmessage = "This is the list of currently available commands:\n\n1. /start - Lists all the available commands!\n2. /joke - Sends you a joke!\n3. /meme - Sends you a meme!\n4. /updates - Shows event updates!\n5. /coronavirus - Fetches you the latest updates about COVID-19!\n\nCheers!"
invalidmessage = "You have selected an invalid option!"
unknownmessage = "Hello there! Don't know where to start? Try using the /start command."
subreddits = [
    "dankmemes",
    "PrequelMemes",
    "politicalhumour",
    "memes",
    "darkmeme",
    "deepfriedmemes",
    "surrealmemes",
    "meme",
    "historymemes",
    "ProgrammerHumor",
    "programminghumor",
    "funny",
    "me_irl",
    "whiteedgymemes",
    "comedyheaven",
    "raimimemes",
    "AdviceAnimals",
    "MemeEconomy",
    "ComedyCemetery",
    "terriblefacebookmemes",
    "teenagers",
    "PewdiepieSubmissions"]
jokeapis = ["https://sv443.net/jokeapi/v2/joke/any",
            "https://sv443.net/jokeapi/v2/joke/dark",
            "https://sv443.net/jokeapi/v2/joke/programming",
            "https://sv443.net/jokeapi/v2/joke/miscellaneous",
            "https://api.chucknorris.io/jokes/random",
            "https://official-joke-api.appspot.com/jokes/random",
            "https://official-joke-api.appspot.com/random_joke",
            "https://api.icndb.com/jokes/random?escape=javascript"]
alreadyregistered = "You have already registered for the event!"
selectusername = "Select a username to display on the leaderboard!"
askanswer = "You think you are smart, don't you? Alright, let's see what you've got!"
submittedall = "You have already submitted all the answers!"
registerfirst = "You must first register for the event!"
firsthint = "Visit our instagram page!"
usernametaken = "Sorry, that username is already taken!"
registersuccess = "You have successfully registered for the event"
gamecompleted = "That is correct! Congratulations! You have successfully answered all the questions!"
wronganswer = "Ping Pong, Sorry, you are wrong!"
coronaapi = "https://pomber.github.io/covid19/timeseries.json"
