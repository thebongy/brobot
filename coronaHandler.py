import requests
from telegram import ReplyKeyboardMarkup
import random
from telegram.ext import ConversationHandler
from text import invalidmessage, coronaapi


def countrySelection(update, context):
    global response
    api = coronaapi
    response = requests.get(api).json()
    countries = list(response.keys())
    randomCountry = random.choice(countries)
    response["World"] = []
    for i in range(len(response[randomCountry])):
        confirmed = 0
        recovered = 0
        deaths = 0
        for j in countries:
            confirmed += response[j][i]["confirmed"]
            deaths += response[j][i]["deaths"]
            recovered += response[j][i]["recovered"]
        response["World"].append({"date": response[randomCountry][i]["date"],
                                  "confirmed": confirmed,
                                  "deaths": deaths,
                                  "recovered": recovered})
    countries = ["World"] + sorted(countries)
    options = []
    for i in range(len(countries)):
        options.append([countries[i]])
    update.message.reply_text(
        "Please select a country name:",
        reply_markup=ReplyKeyboardMarkup(
            options,
            one_time_keyboard=True,
            resize_keyboard=True,
            selective=True))
    return 0


def dateSelection(update, context):
    global country
    country = update.message.text
    if country in response:
        options = []
        for i in response[country]:
            options.append([i['date']])
        options = options[::-1]
        update.message.reply_text(
            "Please select a date:",
            reply_markup=ReplyKeyboardMarkup(
                options,
                one_time_keyboard=True,
                resize_keyboard=True,
                selective=True))
        return 1
    else:
        message = invalidmessage
        update.message.reply_text(message)
        return ConversationHandler.END


def coronaupdates(update, context):
    date = update.message.text
    data = None
    for i in range(len(response[country])):
        if response[country][i]["date"] == date:
            data = response[country][i]
            break
    if data:
        message = f'Cases: {data["confirmed"]}\n'
        message += f'Deaths: {data["deaths"]}\n'
        message += f'Recovered: {data["recovered"]}\n'
        try:
            message += f'New cases on {date}: {data["confirmed"] - response[country][i-1]["confirmed"]}\n'
            message += f'New deaths on {date}: {data["deaths"] - response[country][i-1]["deaths"]}\n'
            message += f'New recovered on {date}: {data["recovered"] - response[country][i-1]["recovered"]}'
        except BaseException:
            message += f'New cases on {date}: {data["confirmed"]}\n'
            message += f'New deaths on {date}: {data["deaths"]}\n'
            message += f'New recovered on {date}: {data["recovered"]}'
    else:
        message = invalidmessage
    update.message.reply_text(message)
    return ConversationHandler.END
