from dotenv import load_dotenv
import os
import pymongo
config = ".env" if os.path.exists(".env") else "sample.env"
load_dotenv(dotenv_path=config)
user = pymongo.MongoClient(
    os.getenv("DB_URL"))["bigbraintime"]["users"]
question = pymongo.MongoClient(
    os.getenv("DB_URL"))["bigbraintime"]["questions"]
